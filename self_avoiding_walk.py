import h5py
import argparse
import time
import numpy as np 
rng = np.random.default_rng()

def initial_config(N):
    """Generates a self avoiding inital config with: 
        dimension of grid d and length N"""
    # Generate walk as a straight line in x-direction 
    # A walk of length N has N+1 points
    x = np.arange(0,N+1, dtype=int)
    y = np.zeros((N+1,1), dtype=int)
    # Concatenation along the second axis 
    walk = np.c_[x,y]
    return walk

def transformation_matrix():
    """Generates a transformation matrix (rotation or reflection) for a 2 dimensional square lattice"""
    rotations_list = [np.array([[0,-1],[1,0]]), np.array([[-1,0],[0,-1]]), np.array([[0,1],[-1,0]])]
    reflections_list = [np.array([[1,0],[0,-1]]), np.array([[0,1],[1,0]]), np.array([[-1,0],[0,1]]), np.array([[0,-1],[-1,0]])]
    transformations_list = rotations_list + reflections_list
    return transformations_list[rng.integers(0,len(transformations_list))]

def sample_pivot(walk): 
    """Proposes a walk by splitting the walk at pivot location in two parts (conserved and changing), 
    Performing matrix multiplication on the changing part of the walk.""" 
    # Generate a random pivot location and transformation matrix 
    location = rng.integers(0, len(walk)-1)
    matrix = transformation_matrix() 

    # Split chain based on pivot location
    conserved_chain = walk[:location+1]
    changing_chain = walk[location+1:] 
     
    # Define origin of rotaion/reflection
    origin = conserved_chain[-1]
    
    # Apply rotation/reflection matrix to changing part of the walk - shift origin of transformation from (0,0) to pivot location
    new_chain = [origin + np.matmul(matrix, np.transpose(i - origin)) for i in changing_chain]
    proposed_walk = np.append(conserved_chain, new_chain, axis=0)

    return proposed_walk 

def acceptance(walk):
    """Checks if the walk is self avoiding by checking if there are only unique coordinates in the walk"""
    # Make a list of all unique elements (coordintates) in walk
    unique_elements = np.unique(walk, axis=0)
    if len(unique_elements) == len(walk): 
        return True
    else: 
        return False

def markov_chain_pivot(walk, m): 
    """Markov chain simulation for m accepted markov-steps - returns a new walk"""
    # Count number of accepted pivots
    step = 0
    while step < m: 
        # Sample a walk
        sample = sample_pivot(walk) 
        # Check if walk is accepted
        if acceptance(sample) == True: 
            # Add to steps and change walk 
            step = step + 1
            walk = sample 
    return walk

def end_to_end_distance(walk): 
    """Computes Euclidian distance between begin and end of the walk"""
    return np.linalg.norm(walk[0] - walk[-1])

def batch_estimate(data,observable,k):
    """Devide data into k batches and apply the function observable to each.
    Returns the mean and standard error."""
    batches = np.reshape(data,(k,-1))
    values = np.apply_along_axis(observable, 1, batches)
    return np.mean(values), np.std(values)/np.sqrt(k-1)

# use the argparse package to parse command line arguments
parser = argparse.ArgumentParser(description='Measures the end-to-end distance for a self-avoiding walk using the pivot-alogrithm')
parser.add_argument('-n', type=int, default=2000, help='length of the walk')
parser.add_argument('-eq', type=int, default=2, help='Number of equilibrium sweeps')
parser.add_argument('-m', type=int, default=50, help='Number of distance measurements per fixed length N')
parser.add_argument('-s', type=float, default=0.5, help='Sweeps between measurements')
parser.add_argument('-o', type=int, default=30, help='Time in seconds between file outputs') 
parser.add_argument('-f', help='Output filename')
args = parser.parse_args()

# perform sanity checks on the arguments
if args.m <= 0: 
    parser.error("Number of measurements needs to be larger than 0") 
if args.s <= 0: 
    parser.error("Sweeps between measurements needs to be larger than 0") 
if args.n <= 0: 
    parser.error("Minimal length of the walk needs to be larger than 0")
if args.eq < 0: 
    parser.error("Minimal number of equilibrium steps needs to be at least 0")

# fix parameters
measurements = args.m
measure_sweeps = args.s
walk_length = args.n
eq_sweeps = args.eq
last_output_time = time.time()

if args.f is None:
    # construct a filename from the parameters plus a timestamp (to avoid overwriting)
    output_filename = "data_n_{}_m_{}_s_{}_t_{}.hdf5".format(walk_length, measurements, measure_sweeps, time.strftime("%Y%m%d%H%M%S"))
else: 
    output_filename = args.f

# measurement phase
with h5py.File(output_filename,'w') as f:
    # create 1D integer data set that is stored in chunks and can be extended
    dataset = f.create_dataset("end-to-end-distances",(measurements, 2), maxshape=(measurements, 2),dtype='float', chunks=True)
    # store some information as metadata for the data set
    dataset.attrs["parameters"] = str(vars(args))
    dataset.attrs["measurements"] = measurements
    dataset.attrs["measure_sweeps"] = measure_sweeps
    dataset.attrs["equilibrium_sweeps"] = eq_sweeps
    dataset.attrs["walk_length"] = walk_length
    dataset.attrs["start time"] = time.asctime()
    dataset.attrs["current_time"] = measurements

walk = initial_config(walk_length) 

#Equilibrating
walk = markov_chain_pivot(walk, walk_length*eq_sweeps)
print('Equilibration done') 

#Measurements
for measurement in range (measurements): 
    if measurement%10 == 0: 
        print('Starting simulation ', measurement, 'of ', measurements) 

    walk = markov_chain_pivot(walk, np.ceil(measure_sweeps*walk_length).astype(int)) 
    distance = end_to_end_distance(walk)

    with h5py.File(output_filename,'a') as f:
        f["end-to-end-distances"][measurement] = [walk_length, distance]
        f["end-to-end-distances"].attrs["current_time"] = time.asctime()
        last_output_time = time.time()