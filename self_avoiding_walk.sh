#!/bin/bash
#SBATCH --partition=hefstud
#SBATCH --output=std_%A_%a.txt
#SBATCH --mem=200M
#SBATCH --time=4-00:00:00
#SBATCH --array=0-9%5
lengths=($(LANG=en_US seq 1000 1000 10000))
len=${lengths[$SLURM_ARRAY_TASK_ID]}
python3 self_avoiding_walk.py -n ${len} -m 250
